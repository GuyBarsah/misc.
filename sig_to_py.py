inputfile_name = r"C:\work\general_scripts\hyd.txt"
outputfile_name = r"C:\work\general_scripts\hyd.py"

#http://www.text-image.com/convert/ascii.html

with open(inputfile_name) as inputfile:
    outputfile = open(outputfile_name, 'w')
    for line in inputfile:
        clean_line = line.replace('\n', '')
        new_line = "msg += \'{}\'".format(clean_line) + r" + '\n'" + '\n'
        outputfile.write(new_line)
    outputfile.close()
    print "output in: {}".format(outputfile_name)
