# PARAMETRES
input_file = r"C:\work\hydralisk\MODEL_Model_64GB_Bics3_2FIMS_256G_1DIE_RACK36_10_Test_D_FEX12_12012018105047_0.log"
slices = 5

if __name__ == '__main__':
    import datetime


    def get_sig():
        msg = ''
        msg += '                                                   /ys/.                                            ' + '\n'
        msg += '                                                     /mMNy/                                         ' + '\n'
        msg += '                                                       +NMMMh+                                      ' + '\n'
        msg += '                                                         yMMMMNh:                                   ' + '\n'
        msg += '                             :::///:::                    sMMMMMMm+                                 ' + '\n'
        msg += '                       /osydmMMMMMMMMMMMNNmho:             mMMMMMMMNo         Nh                    ' + '\n'
        msg += '                              :ohNMMMMMMMMMMMMNd+          sMMMMMMMMMm        +MMo                  ' + '\n'
        msg += '                                  :yMMMMMMMMMMMMMNy        +MMMMMMMMMMN/       NMMy                 ' + '\n'
        msg += '                                     hMMMMMMMMMMNhs+       sMMMMMMMMMMMM:      hMMMd                ' + '\n'
        msg += '                                      sMMMMMMMN/           NMMMMMMMMMMMMm      yMMMMd               ' + '\n'
        msg += '                                       dMMMMMMo           hMMMMMMMMMmdhhd:     mMMMMMh              ' + '\n'
        msg += '                +shmNNNNNNNNNNd+        MMMMMMs          dMMMMMMMMh           /MMMMMMM+             ' + '\n'
        msg += '           :ohNNMMMMMMMMMMMMMMd         NMMMMMMs      /hNMMMMMMMMm            mMMMMMMMN             ' + '\n'
        msg += '         smNMMMMMMMMMMMMMMMMMM+         NMMMMMMMNNmmNNMMMMMMMMMMMh           hMMMMMMMMMo            ' + '\n'
        msg += '      /dNNmmddmmNMMMMMMMMMMMMMm:       yMMMMMMMMMMMMMMMMMMMMMMMMMN         :mMMMMMMMMMMd     +      ' + '\n'
        msg += '     ::           :/shmMMMMMMMMNdsoosdNMMMMMMMMMMMMMMMMMMMMMMMMMMMNs:    /yNMMMMMMMMMMMm     N      ' + '\n'
        msg += '                        oNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNNNNMMMMMMMMMMMMMMm    yM+     ' + '\n'
        msg += '               :++        NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmhysyhmy    MMy     ' + '\n'
        msg += '           /ymNMMo        dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+           dMMd     ' + '\n'
        msg += '        :yNMMMMMMo        NMMMMMMMMMMMMMMMMMNdyo+:   :/oydNMMMMMMMMMMMMMMMMMMMs           hMMMd     ' + '\n'
        msg += '      :hMMMMMMMMMNs:   /omMMMMMMMMMMMMMMNho:               /hNMMMMMMMMMMMMMMMMy         +NMMMMy     ' + '\n'
        msg += '     yMMMMMMMMMMMMMMNNNMMMMMMMMMMMMMMNh+                      omMMMMMMMMMMMMMMMy:     omMMMMMMo     ' + '\n'
        msg += '   +NMMMMMMNmmNMMMMMMMMMMMMMMMMMMMMNs                           sMMMMMMMMMMMMMMMMmmdmNMMMMMMMM:     ' + '\n'
        msg += '  oMMMNds/     :smMMMMMMMMMMMMMMMNs                              /NMMMMMMMMMMMMMMMMMMMMMMMMMMm     /' + '\n'
        msg += ' +MMd+            hMMMMMMMMMMMMMh           /ohdmmmmmds/          :NMMMMMMMMMMMMMMMMMMMMMMMMM/    /m' + '\n'
        msg += ' Nd/               MMMMMMMMMMMMs          ymMMMMMMMMMMMMm+         /MMMMMMMMMMMMMMMMMmhsyhmNs    +Nh' + '\n'
        msg += 's+      yo        sMMMMMMMMMMMy         hMMMMMMMMMMMMMMMMMy         hMMMMMMMMMMMMMMN/           sMM+' + '\n'
        msg += '      /mMMd+    +dMMMMMMMMMMMh        oNMMMMMMMMMMMMMMMMMMM/        /MMMMMMMMMMMMMMy           dMMN ' + '\n'
        msg += '     oMMMMMMMNNNMMMMMMMMMMMMm        oMMMMMMMMMMNmdmmMMMMMMh         MMMMMMMMMMMMMMN         yNMMMo ' + '\n'
        msg += '    sMMMMMMMMMMMMMMMMMMMMMMM/        MMMMMMMMNy       yMMMMh         MMMMMMMMMMMMMMMNho+++sdNMMMMm  ' + '\n'
        msg += '   +MMMMMMNmmNMMMMMMMMMMMMMN        oMMMMMMMN          hMMN         yMMMMMMMMMMMMMMMMMMMMMMMMMMMM   ' + '\n'
        msg += '   NMMMms      omMMMMMMMMMMm        hMMMMMMMy          oMm/         NMMMMMMMMMMMMMMMMMMMMMMMMMMMo   ' + '\n'
        msg += '  oMMm+          mMMMMMMMMMm        dMMMMMMMs          y+          mMMMMMMMMMMMMMMMMMMMMMMMMMMMy    ' + '\n'
        msg += '  dMh            sMMMMMMMMMm        yMMMMMMMm                     mMMMMMMMMMMMMMMMMMMMMMMMMMMMh     ' + '\n'
        msg += '  Ny             NMMMMMMMMMM        +MMMMMMMMh                  hMMMMMMMMMMMMMMMMMMMMMMMMMMMMh      ' + '\n'
        msg += '  y     y/    +yNMMMMMMMMMMMs        mMMMMMMMMNy/            +hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMy       ' + '\n'
        msg += '       +MMMMMMMMMMMMMMMMMMMMN         NMMMMMMMMMMNdhso++oshdNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+        ' + '\n'
        msg += '       yMMMMMMMMNNMMMMMMMMMMMd         mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm          ' + '\n'
        msg += '       dMMMMMms/  /odMMMMMMMMMh         hMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMy           ' + '\n'
        msg += '       dMMMMy        +MMMMMMMMMy          dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm/            ' + '\n'
        msg += '       dMMMy          dMMMMMMMMMh          /dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNo              ' + '\n'
        msg += '       yMMh           NMMMMMMMMMMd            yNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNs                ' + '\n'
        msg += '       /MM     o    oNMMMMMMMMMMMMN+            /ymNMMMMMMMMMMMMMMMMMMMMMMMMMMMMNs                  ' + '\n'
        msg += '        mm    sMMMMMMMMMMMMMMMMMMMMMh               odNMMMMMMMMMMMMMMMMMMMMMMMNs                    ' + '\n'
        msg += '        /y    sMMMMMMMMNNmmNNMMMMMMMMMy                 +ymNMMMMMMMMMMMMMMMMmo                      ' + '\n'
        msg += '               NMMMMMy       /sNMMMMMMMMh/                   +ydNNMMMMMMMMd/                        ' + '\n'
        msg += '               +MMMM/           sMMMMMMMMMmo                      /+ydNNy                           ' + '\n'
        msg += '                +MMm             /MMMMMMMMMMNy                                                      ' + '\n'
        msg += '                  mN              dMMMMMMMMMMMMms                                                   ' + '\n'
        msg += '                   +s             hMMMMMMMMMMMMMMMNho                                               ' + '\n'
        msg += '                                  MMMMMMMMMMMMMMMMMMMMMmho                                          ' + '\n'
        msg += '            o+                  /NMMMMMMMMMMMMMMMMMMMMMMMy                                          ' + '\n'
        msg += '             /mho             omMMMMMMMMMMMMMMMMMMMMMMmo                                            ' + '\n'
        msg += '               +dMMNdhyysyhmNMMMMMMMMMMMMMMMMMMMMMMdo                                               ' + '\n'
        msg += '                  +ymMMMMMMMMMMMMMMMMMMMMMMMMMNho/                                                  ' + '\n'
        msg += '                       /oshdmNNMMMMMMMNmdyo+     ' + '\n'
        msg += 'Hydralisk' + '\n'
        msg += 'By Guy Barash' + '\n'
        msg += '\n'
        return msg


    print get_sig()
    start_time = datetime.datetime.now()

if __name__ == '__main__':
    from itertools import takewhile, repeat

    from_file = open(input_file, 'rb')
    amount_of_lines = len(from_file.readlines())
    from_file.close()
    print "Number of lines: {}".format(amount_of_lines)

    slice_size = amount_of_lines / slices
    start_locations = range(0, amount_of_lines, slice_size)
    locations = [(x, min([(x + slice_size - 1), amount_of_lines - 1])) for x in start_locations]

if __name__ == '__main__':
    run_params = zip(range(slices), [input_file] * slices, locations)


def runner(run_params):
    import os
    doc_idx = run_params[0]
    input_file = run_params[1]
    start_idx = run_params[2][0]
    end_idx = run_params[2][1]

    filename, file_extension = os.path.splitext(input_file)
    to_file_name = '{}_prt{}{}'.format(filename, doc_idx + 1, file_extension)

    from_file = open(input_file, 'rb')
    to_file = open(to_file_name, 'w')

    curr_line = -1
    for line in from_file:
        curr_line += 1
        if curr_line >= start_idx:
            to_file.write(line.replace('\n', ''))
            if curr_line >= end_idx:
                break

    from_file.close()
    to_file.close()


if __name__ == '__main__':
    import psutil
    from multiprocessing import Pool

    cpu_count = psutil.cpu_count()
    pool = Pool(processes=cpu_count)
    results = pool.imap(runner, run_params)
    pool.close()
    pool.join()

if __name__ == '__main__':
    duration = datetime.datetime.now() - start_time
    print "Run completed, total time: {}".format(str(duration))
