if __name__ == '__main__':
    skip = True
    skip_c = False
    force = True
    import nltk
    import random, os
    from nltk.corpus import wordnet

    print ""
    print "Start Of Code."
    print "<------------>"
    print ""

if __name__ == '__main__':
    if not skip_c:
        given_word = 'ship'
        synt = wordnet.synsets(given_word)
        synons = list()
        print "{}:".format(given_word)
        for idx, defn in enumerate(synt):
            print "{:>3})\t{} [{}]".format(idx + 1, defn.definition(), defn.examples())
            for lemma in defn.lemmas():
                synons.append(lemma.name())

        print ''
        print "Synonyms: {}".format(list(set(synons)))

if __name__ == '__main__':
    if not skip_c:
        w1 = wordnet.synset('ship.n.1')
        w2 = wordnet.synset('boat.n.1')
        simil = w1.wup_similarity(w2)
        print "{} and {} are {:>.2f}%".format(w1, w2, simil * 100)

        w2 = wordnet.synset('cat.n.1')
        simil = w1.wup_similarity(w2)
        print "{} and {} are {:>.2f}%".format(w1, w2, simil * 100)

        w2 = wordnet.synset('ocean.n.1')
        simil = w1.wup_similarity(w2)
        print "{} and {} are {:>.2f}%".format(w1, w2, simil * 100)

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
