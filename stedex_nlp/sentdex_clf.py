if __name__ == '__main__':
    skip = True
    skip_c = False
    force = True
    import nltk
    import random, os

    print ""
    print "Start Of Code."
    print "<------------>"
    print ""

if __name__ == '__main__':
    from nltk.corpus import movie_reviews
    import cPickle as pickle

    pickle_path = r'C:\work\general_scripts\stedex_nlp\db_1.p'

    if os.path.exists(pickle_path) and not force:
        with open(pickle_path, "rb") as fin:
            metadata = pickle.load(fin)

        all_words = metadata['all_words']
        all_words_freq = metadata['all_words_freq']
        docs = metadata['docs']


    else:

        docs = [(list(movie_reviews.words(fileid)), label)
                for label in movie_reviews.categories()
                for fileid in movie_reviews.fileids(label)[:200]
                ]
        random.shuffle(docs)

        all_words = [w.lower() for w in movie_reviews.words()]
        all_words_freq = nltk.FreqDist(all_words)

        metadata = dict()
        metadata['all_words'] = all_words
        metadata['all_words_freq'] = all_words_freq
        metadata['docs'] = docs
        with open(pickle_path, "wb") as fout:
            pickle.dump(metadata, fout, protocol=pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
    def find_features(doc, words_freq):
        word_feature = [x[0] for x in words_freq.most_common(1000)]
        words = set(doc)
        features = dict()
        for w in word_feature:
            features[w] = (w in words)
        return features


    pickle_path = r'C:\work\general_scripts\stedex_nlp\db_2.p'
    if os.path.exists(pickle_path) and not force:
        with open(pickle_path, "rb") as fin:
            metadata = pickle.load(fin)
        feature_set = metadata['feature_set']

    else:
        feature_set = [(find_features(doc[0], all_words_freq), label) for (doc, label) in docs]

        metadata['feature_set'] = feature_set
        with open(pickle_path, "wb") as fout:
            pickle.dump(metadata, fout, protocol=pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
    train_size = 50
    training_set = feature_set[:train_size]
    testing_set = feature_set[train_size:]

    clf = nltk.NaiveBayesClassifier.train(training_set)
    # accuracy = nltk.classify.accuracy(clf, testing_set)
    # print "Accuracy: {:.02f}%".format(accuracy * 100)
    clf.show_most_informative_features(16)

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
