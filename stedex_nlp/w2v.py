import warnings

warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
import gensim

# Load Google's pre-trained Word2Vec model.
model = gensim.models.KeyedVectors.load_word2vec_format(
    r"C:\Anaconda2\GoogleNews-vectors-negative300.bin\GoogleNews-vectors-negative300.bin",
    binary=True)

print "Completed"
