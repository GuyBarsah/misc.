if __name__ == '__main__':
    print ""
    print "Start Of Code."
    print "<------------>"
    print ""

if __name__ == '__main__':
    skip = True
    skip_c = False

    ex_1 = ''
    ex_1 += "Hello Mr. Barash, how are you today? The weather is great, Python is awsone. The skies are blue. "
    ex_1 += "You really shouldn't eat sand."

    ex_2 = ''
    ex_2 += "This is a great example of showing off some stop word filtration"

# word/sentences tokenizer
if __name__ == '__main__':
    from nltk.tokenize import sent_tokenize, word_tokenize

    if not skip:

        print "Sentence tokenize:"
        for i, v in enumerate(sent_tokenize(ex_1)):
            print "{:>3})\t{}".format(i, v)
        print ""

        print "word tokenize:"
        for i, v in enumerate(word_tokenize(ex_1)):
            print "{:>3})\t{}".format(i, v)
        print ""

# Stop words
if __name__ == '__main__':
    from nltk.corpus import stopwords

    stop_words = set(stopwords.words("english"))

    if not skip:
        filtered_ex_2 = [w for w in word_tokenize(ex_2) if not w in stop_words]
        print "Filtering out common stop words:"
        print filtered_ex_2
        print ""

# Stemming
if __name__ == '__main__':
    from nltk.stem import PorterStemmer

    ps = PorterStemmer()

    if not skip:
        example_words = ['python', 'pythoner', 'pythonning', 'pythoned', 'pythonly']
        example_text = ''
        example_text += 'It is very important to be pythonly while you are pythoning with python. '
        example_text += 'All pythoners have pythoned poorly at least once in their life.'

        stem_words = [ps.stem(w) for w in word_tokenize(example_text) if w not in stop_words]
        for w in stem_words:
            print w

# NER (Named Entity recognition)
if __name__ == '__main__':
    if not skip:
        import nltk
        from nltk.tokenize import sent_tokenize, word_tokenize
        from nltk.stem import PorterStemmer

        ps = PorterStemmer()

        ex_1 = ''
        # ex_1 += "Hello Mr. Barash, how are you today? The weather is great, Python is awsome. The skies are blue. "
        # ex_1 += "You really shouldn't eat sand. "
        ex_1 += 'Did the blue smurfs managed to escape the great garagamel?'

        stop_words = set(stopwords.words("english"))
        sentences = sent_tokenize(ex_1)

        for sent in sentences:
            sent_p = sent
            tagged = nltk.pos_tag(word_tokenize(sent_p))

            named_ent = nltk.ne_chunk(tagged, binary=True)
            print "{}".format(tagged)
            print "<------------>"

if __name__ == '__main__':
    if not skip_c:
        import nltk
        from nltk.stem import WordNetLemmatizer

        lemmatizer = WordNetLemmatizer()

        ex_1 = ''
        ex_1 += "Hello Mr. Barash, how are you today? The weather is great, Python is awsome. The skies are blue. "
        ex_1 += "where did the cats go?"
        ex_1 += 'Did the blue smurfs managed to escape the great garagamel?'

        filtered_ex_1 = [w for w in word_tokenize(ex_1) if not w in stop_words]
        for word in filtered_ex_1:
            lema_word_v = lemmatizer.lemmatize(word, pos='v')
            lema_word_a = lemmatizer.lemmatize(word, pos='a')
            lema_word_n = lemmatizer.lemmatize(word)
            if word != lema_word_v:
                print "[VERB] {:^15} <--> {:^15}".format(word, lema_word_v)
            elif word != lema_word_a:
                print "[ADJ ] {:^15} <--> {:^15}".format(word, lema_word_a)
            elif word != lema_word_n:
                print "[NOUN] {:^15} <--> {:^15}".format(word, lema_word_n)


if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
