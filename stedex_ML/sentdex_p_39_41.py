import numpy as np
import pandas as pd
from sklearn.cluster import KMeans, MeanShift
from sklearn import preprocessing, model_selection, decomposition
from sklearn.datasets.samples_generator import make_blobs

import warnings, random
from matplotlib import style
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from collections import Counter

style.use('ggplot')

# if __name__ == '__main__':
#     centers = [
#         [1, 1, 1],
#         [5, 5, 5],
#         [3, 10, 10],
#         [1, 1, 9],
#     ]
#     X, xe = make_blobs(100, centers=centers, cluster_std=0.5)

if __name__ == '__main__':
    titanic_path = r"C:\Users\32519\Downloads\titanic.xls"

    df = pd.read_excel(titanic_path)
    original_df = pd.DataFrame.copy(df)
    df.drop(['body', 'name', 'ticket', 'boat'], 1, inplace=True)
    df = df.apply(pd.to_numeric, errors='ignore')


    def column_to_numeric(col_name, df):
        if df[col_name].dtype in [np.int64, np.float64]:
            return df

        col_content = df[col_name].values.tolist()
        col_content_set = set(col_content)

        translation_dict = dict()
        idx = 0
        for val in col_content_set:
            translation_dict[val] = idx
            idx += 1

        def val_to_numeric(v):
            return translation_dict[v]

        df[col_name] = map(val_to_numeric, df[col_name])
        return df


    # Convert all values to numeric
    for col in df.columns.values:
        df = column_to_numeric(col, df)

    df.fillna(0, inplace=True)
    y = df['survived']
    df.drop(['survived'], 1, inplace=True)

    X = np.array(df).astype(float)
    X = preprocessing.scale(X)

if __name__ == '__main__':
    # clf = MeanShift()
    clf = KMeans(n_clusters=10)
    clf.fit(X)
    labels = clf.labels_
    cluster_centers = clf.cluster_centers_
    n_clusters = len(np.unique(labels))
    print "Number of clusters: {}".format(n_clusters)


    def best_fit(labels, y):
        ret_list = dict()
        n_clusters = len(np.unique(labels))
        total_hits = 0
        total_items = len(labels)

        for idx in range(len(labels)):
            clabel = labels[idx]
            cpredict = y[idx]
            ret_list[clabel] = ret_list.get(clabel, list()) + [cpredict]

        ret_labels = dict()
        ret_acc = dict()
        items_in_cluster = dict()
        for cluster, vlist in ret_list.iteritems():
            acc = np.mean(vlist)
            decision = int(round(acc))
            items = len(vlist)
            chits = max(sum(vlist), items - sum(vlist))
            total_hits += chits

            ret_acc = max(acc, 1 - acc)
            ret_labels[cluster] = decision
            items_in_cluster[cluster] = items
            print "Cluster {:>3} is {:>3}.\titems: {:>4}\t\tAccuracy: {:>6.2f}%".format(cluster,
                                                                                        decision,
                                                                                        items,
                                                                                        ret_acc * 100)
        print ""
        print "Total accuracy: {:>6.2f}%".format(float(total_hits) / total_items)

        return ret_labels, ret_acc


    translation = best_fit(labels, y)
    # print ""
    # print_group = 3
    # original_df['cluster'] = labels
    # print "People of group {}".format(print_group)
    # print df[original_df['cluster'] == print_group]

if __name__ == '__main__':
    project_pca = False
    if project_pca:
        # Show 2D PCA:
        pca = decomposition.PCA()
        pca_components = 2
        pca.n_components = pca_components
        X_reduced = np.array(pca.fit_transform(X))

        colors = ['r', 'b']
        mark = ['o', 'x']
        for idx in range(len(X_reduced)):
            p = X_reduced[idx]
            label = y[idx]
            plt.scatter(p[0], p[1], c=colors[label], marker=mark[label])
        plt.show()

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
