import pandas as pd
import quandl as Quandl
import numpy as np
import math
import cPickle as pickle

from sklearn import preprocessing, svm, model_selection
from sklearn.linear_model import LinearRegression

if __name__ == '__main__':
    df = Quandl.get('WIKI/GOOGL')

if __name__ == '__main__':
    df['HL_PCT'] = (df['Adj. High'] - df['Adj. Low']) / df['Adj. Close'] * 100
    df['HL_PCT_change'] = (df['Adj. Close'] - df['Adj. Open']) / df['Adj. Open'] * 100
    df = df[['Adj. Close', 'HL_PCT', 'HL_PCT_change', 'Adj. Volume']]

if __name__ == '__main__':
    forcast_col = 'Adj. Close'
    df.fillna(-99999, inplace=True)

    forcast_out = int(math.ceil(0.01 * len(df)))
    df['label'] = df[forcast_col].shift(-forcast_out)
    df.dropna(inplace=True)

if __name__ == '__main__':
    X = np.array(df.drop(['label'], 1))
    X = preprocessing.scale(X)

    X_lately = X[-forcast_out:]
    X = X[:-forcast_out:]

    y = np.array(df['label'])
    y_lately = y[-forcast_out:]
    y = y[:-forcast_out]

if __name__ == '__main__':
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)
    clf = LinearRegression()
    clf.fit(X_train, y_train)
    accuracy = clf.score(X_test, y_test)
    print "Accuracy: {:>.2f}".format(accuracy)

    with open('C:\\linearreg.p', 'wb') as ffile:
        pickle.dump(clf, ffile)

if __name__ == '__main__':
    forcast_set = clf.predict(X_lately)

    print "{:^10}{:^10}{:^10}".format("PREDICT", "ACTUAL", "DELTA(%)")
    for i in xrange(forcast_set.shape[0]):
        a = forcast_set[i]
        b = y_lately[i]
        print "{:^10.2f}{:^10.2f}{:^10.2f}".format(a, b, 100 * math.fabs(a - b) / b)

if __name__ == '__main__':
    pass

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
