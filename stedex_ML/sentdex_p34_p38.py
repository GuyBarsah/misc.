import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn import preprocessing, model_selection, decomposition

import warnings, random
from matplotlib import style
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from collections import Counter

style.use('ggplot')

if __name__ == '__main__':
    X = np.array([
        [1, 2],
        [1.5, 1.8],
        [5, 8],
        [8, 8],
        [1, 0.6],
        [9, 11],

        [2.3, 1.2],
        [1, 1],
        [8.1, 6.9],
        [8, 9.3],
        [0.2, 3.2],
        [10, 10],
    ])


class Kmean:
    def __init__(self, n_clusters=2, tol=0.001, max_iter=300):
        if __name__ == '__main__':
            self.k = n_clusters
            self.tol = tol
            self.max_iter = max_iter

    def fit(self, X):
        self.centroids = dict()

        seeds = random.sample(X, self.k)
        for ki in range(self.k):
            self.centroids[ki] = seeds[ki]

        for iter in range(self.max_iter):
            self.classification = dict()
            for ki in range(self.k):
                self.classification[ki] = list()

            for feature_set in X:
                distances = [np.linalg.norm(feature_set - self.centroids[centoroid]) for centoroid in self.centroids]
                label = distances.index(min(distances))
                self.classification[label].append(feature_set)

            prev_centroids = dict(self.centroids)

            for classification in self.classification:
                self.centroids[classification] = np.average(self.classification[classification], axis=0)

            optimized = True
            for c in self.centroids:
                original_c = prev_centroids[c]
                current_c = self.centroids[c]
                dist = np.sum((current_c - original_c) / original_c * 100)
                if dist > self.tol:
                    optimized = False

            if optimized:
                break

        # print "Completed after {} iterations".format(iter + 1)
        self.labels_ = self.predict(X)
        self.cluster_centers_ = np.array(self.centroids.values())

    def predict(self, X):
        labels = list()
        for feature_set in X:
            distances = [np.linalg.norm(feature_set - self.centroids[centoroid]) for centoroid in self.centroids]
            label = distances.index(min(distances))
            labels.append(label)
        return labels


# if __name__ == '__main__':
#     fig_all, fig_pointers = plt.subplots(2, 3)
#     total_k = 6
#     for idx_k in range(total_k):
#         k = idx_k + 1
#         row = idx_k % 3
#         col = idx_k / 3
#         fig = fig_pointers[col][row]
#         fig.set_title("K = {}".format(k))
#         colors = cm.rainbow(np.linspace(0, 1, k))
#         clf = Kmean(k=k)
#         clf.fit(X)
#         labels = clf.labels_
#         centoroids = clf.cluster_centers_
#
#         for idx in range(len(X)):
#             point = X[idx]
#             label = labels[idx]
#             color = colors[label]
#             fig.scatter(point[0], point[1], c=color, s=150)
#
#         fig.scatter(centoroids[:, 0], centoroids[:, 1], marker='x', linewidths=90, c='black')
#     plt.show()
#     plt.close('all')

if __name__ == '__main__':
    titanic_path = r"C:\Users\32519\Downloads\titanic.xls"
    df = pd.read_excel(titanic_path)

    df.drop(['body', 'name', 'ticket', 'boat'], 1, inplace=True)
    df = df.apply(pd.to_numeric, errors='ignore')


    def column_to_numeric(col_name, df):
        if df[col_name].dtype in [np.int64, np.float64]:
            return df

        col_content = df[col_name].values.tolist()
        col_content_set = set(col_content)

        translation_dict = dict()
        idx = 0
        for val in col_content_set:
            translation_dict[val] = idx
            idx += 1

        def val_to_numeric(v):
            return translation_dict[v]

        df[col_name] = map(val_to_numeric, df[col_name])
        return df


    # Convert all values to numeric
    for col in df.columns.values:
        df = column_to_numeric(col, df)

    df.fillna(0, inplace=True)
    y = df['survived']
    df.drop(['survived'], 1, inplace=True)

    X = np.array(df).astype(float)
    X = preprocessing.scale(X)

if __name__ == '__main__':
    project_pca = False
    if project_pca:
        # Show 2D PCA:
        pca = decomposition.PCA()
        pca_components = 2
        pca.n_components = pca_components
        X_reduced = np.array(pca.fit_transform(X))

        colors = ['r', 'b']
        mark = ['o', 'x']
        for idx in range(len(X_reduced)):
            p = X_reduced[idx]
            label = y[idx]
            plt.scatter(p[0], p[1], c=colors[label], marker=mark[label])
        plt.show()

        # # Show 3D PCA:
        # from mpl_toolkits.mplot3d import Axes3D
        # pca = decomposition.PCA()
        # pca_components = 3
        # pca.n_components = pca_components
        #
        # fig = plt.figure()
        # ax = fig.add_subplot(111, projection='3d')
        #
        # X_reduced = np.array(pca.fit_transform(X))
        # colors = ['r', 'b']
        # mark = ['o', 'x']
        # for idx in range(len(X_reduced)):
        #     p = X_reduced[idx]
        #     label = y[idx]
        #     plt.scatter(p[0], p[1], p[2], c=colors[label], marker=mark[label])
        # plt.show()

if __name__ == '__main__':
    clfs = {'SKLEARN': KMeans(n_clusters=2),
            'HomeMade': Kmean(n_clusters=2)}

    for clf_name, clf in clfs.iteritems():
        attempts = 20
        accuracy_list = list()
        for attempt in range(attempts):

            clf.fit(X)


            def flip_needed(labels, y):
                corrects = float(sum([labels[i] == y[i] for i in range(len(labels))]))
                total = len(labels)
                win_rate = corrects / total

                if win_rate > 0.5:
                    # print "No flip needed"
                    return labels, win_rate
                else:
                    # print "Flipped"
                    labels = [1 - yp for yp in labels]
                    return labels, 1 - win_rate


            labels, accuracy = flip_needed(clf.labels_, y)
            accuracy_list.append(accuracy)

        print "Accuracy[{}]: {:>.2f}%".format(clf_name, np.mean(accuracy_list) * 100)

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
