from statistics import mean
import numpy as np
import matplotlib.pyplot as plt
import random

if __name__ == '__main__':
    def create_data(hm, var, step=2, correlation=False):
        yval = 1
        xval = 0
        ys = list()
        xs = list()
        for i in range(hm):
            xs.append(xval)
            xval += 1
            y = yval + random.randrange(-var, var)
            ys.append(y)
            if correlation and correlation == 'pos':
                yval += step
            elif correlation and correlation == 'neg':
                yval -= step

        return np.array(xs, dtype=np.float64), np.array(ys, dtype=np.float64)


    # xs = range(1, 7, 1)
    # xs = np.array(xs, dtype=np.float64)
    #
    # ys = [4, 5, 6, 5, 6, 7]
    # ys = np.array(ys, dtype=np.float64)

    xs, ys = create_data(40, 20, 2, 'neg')

if __name__ == '__main__':
    def best_fit_slope(x, y):
        x_mean = mean(x)
        xx_mean = mean(x ** 2)
        y_mean = mean(y)
        xy_mean = mean(x * y)

        m = ((x_mean * y_mean) - xy_mean) / ((x_mean ** 2) - xx_mean)
        b = y_mean - m * x_mean
        return m, b


    m, b = best_fit_slope(xs, ys)
    regression_line = m * xs + b
    print "Best fit: 'Y = {:.2f}X + {:.2f}'".format(m, b)

if __name__ == '__main__':
    se_y_pred = sum(((regression_line - ys) ** 2))
    se_y_avg = sum(((mean(ys) - ys) ** 2))
    r_sq = 1 - (se_y_pred / se_y_avg)
    e = r_sq
    print "R^2 = {}".format(e)

if __name__ == '__main__':
    predict_x = 8
    predict_y = (m * predict_x) + b

    m, b = best_fit_slope(xs, ys)
    regression_line = m * xs + b

if __name__ == '__main__':
    from matplotlib import style

    plt.title(r'$R^2={:.2f}$'.format(e))
    plt.scatter(xs, ys, c='b')
    plt.scatter(predict_x, predict_y, c='r')
    plt.plot(xs, regression_line)
    plt.show()

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
