import numpy as np
import pandas as pd
from sklearn import preprocessing, model_selection, neighbors

import warnings
from matplotlib import style
import matplotlib.pyplot as plt
from collections import Counter

style.use('fivethirtyeight')
print ''

if __name__ == '__main__':
    # data set can be found in: https://archive.ics.uci.edu/ml/datasets.html
    df = pd.read_csv(r"C:\Repos\learn\breast-cancer-wisconsin.data")
    df.replace('?', -9999, inplace=True)  # Unknown data
    df.drop(['id'], 1, inplace=True)  # remove ID colum
    df = df.astype('float')

    X = np.array(df.drop(['class'], 1))
    y = np.array(df[['class']])
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)

if __name__ == '__main__':

    k_axis = list()
    acc_axis = list()
    repeats = 500

    for k in range(1, 70):
        accuracy = list()
        for tx in range(repeats):
            clf = neighbors.KNeighborsClassifier(k)
            clf.fit(X_train, y_train.ravel())
            accuracy.append(clf.score(X_test, y_test.ravel()))
        accuracy_v = np.mean(accuracy)
        k_axis.append(k)
        acc_axis.append(accuracy_v * 100)
        # print "SKLEARN Accuracy[k={}]: {:>.2f}%".format(k, accuracy_v * 100)

    p_id = acc_axis.index(max(acc_axis))
    py = acc_axis[p_id]
    px = k_axis[p_id]

    plt.plot(k_axis, acc_axis, marker='o')
    plt.annotate('K={}, Accuracy={:>.1f}'.format(px, py), xy=(px, py), xytext=(px, py * 1.01))
    plt.scatter(px, py, c='b')

    plt.ylim([80, 100])
    plt.ylabel('Accuracy')
    plt.xlabel("K")
    plt.show()
#
# if __name__ == '__main__':
#     example_predict = [[4, 2, 1, 1, 1, 2, 3, 2, 1], [4, 2, 1, 2, 2, 2, 3, 2, 1]]
#     example_predict_np = np.array(example_predict)
#
#     prediction = clf.predict(example_predict_np)
#     print "Prediction: {}".format(prediction)

if __name__ == '__main__':
    def knn(X_train, y_train, X_test, y_test, k=5):
        from scipy.spatial import distance
        hits = list()
        if len(X) < k:
            print "ERROR in K number"
            return None
        else:
            for p_i in range(len(X_test)):
                p = X_test[p_i]
                distances = list()
                for sample_point in X_train:
                    dist_vec = distance.euclidean(p, sample_point)
                    distances.append(dist_vec)
                distances = np.array(distances)
                best_fit_idx = np.argsort(distances)[:k]
                votes = [y_train[i][0] for i in best_fit_idx]
                prediction = Counter(votes).most_common(1)[0][0]
                confidence = Counter(votes).most_common(1)[0][1] / float(k)
                actual = y_test[p_i][0]
                correct = actual == prediction
                hits.append(correct)

        accuracy = np.mean(hits)
        return accuracy


    result = knn(X_train, y_train, X_test, y_test)
    print "Our accuracy (k=3): {:>.2f}%".format(result * 100)

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
