import numpy as np
import pandas as pd
from sklearn import preprocessing, model_selection, svm

import warnings
from matplotlib import style
import matplotlib.pyplot as plt
from collections import Counter

style.use('ggplot')

if __name__ == '__main__':
    # data set can be found in: https://archive.ics.uci.edu/ml/datasets.html
    df = pd.read_csv(r"C:\Repos\learn\breast-cancer-wisconsin.data")
    df.replace('?', -9999, inplace=True)  # Unknown data
    df.drop(['id'], 1, inplace=True)  # remove ID colum
    df = df.astype('float')

    X = np.array(df.drop(['class'], 1))
    y = np.ravel(np.array(df[['class']]))
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)

if __name__ == '__main__':
    clf = svm.SVC()
    clf.fit(X_train, y_train)
    accuracy = clf.score(X_test, y_test)
    print "SVM accuracy: {:>.2f}%".format(accuracy * 100)

if __name__ == '__main__':
    print ""
    print "<------------>"
    print "End Of Code."
